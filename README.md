import pandas as pd
import seaborn as sns , numpy as np
import xlrd
import matplotlib.pyplot as plt
excel_file =  'F:\Level 3\Selected#1\Project\Facebook_metrics\Facebook.xls'
data = pd.read_excel(excel_file)
################### Rug Plot #########################
sns.distplot(data['Total Interactions'], rug=True, hist=False)
plt.show()
################## plot scatter #####################
#plot scatter between Post Month and Interactions People
data.plot(kind="scatter", x="Post Month", y="Total Interactions")
plt.show()
#plot scatter between Post weakly and Interactions People
data.plot(kind="scatter", x="Post Weekday", y="Total Interactions")
plt.show()
#plot scatter between Post Month and Interactions People
data.plot(kind="scatter", x="Post Hour", y="Total Interactions")
plt.show()
###################	Histogram Plot ##################
n, bins, patches = plt.hist(x=data['Page total likes'], bins='auto', color='#0504aa',alpha=0.7, rwidth=0.55)
plt.grid(axis='y', alpha=0.75)
plt.show()

################## Box Plot ##############
ax = sns.boxplot(x="Type", y="Total Interactions", data=data)
plt.show()

#sns.distplot(data['Total Interactions'], rug=True, hist=False)

print(data['Total Interactions'].mean())
print(data['Total Interactions'].std())
sample_data= data.sample(n=50)
print(sample_data['Total Interactions'].mean())
print(sample_data['Total Interactions'].std())
print(data[['Post Month','Total Interactions']].cov())
print(data[['Post Weekday','Total Interactions']].cov())
print(data[['Post Hour','Total Interactions']].cov())

#print(np.cov(data['Post Weekday'],data['Total Interactions']))

